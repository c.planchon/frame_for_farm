import bpy

class FFF_PT_Panel(bpy.types.Panel):
    """Creates a Panel in the Tool Shelf"""
    bl_label = "Frame For Farm"
    bl_idname = "FFF_PT_Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "render"
    
    @classmethod
    def poll(cls, context):
        return True

    def draw(self, context):
        layout = self.layout
        props = context.window.scene.fffprops
        row = layout.row()
        row.prop(props, "splits_dir")
        row = layout.row()
        layout.prop(props, "parts")
        row = layout.row()
        row.operator("fff.prepare")
        row = layout.row()
        row.operator("fff.assemble")
        row = layout.row()
        row.label(text="Scene is animated:")
        row.prop(props, "animated")
        row = layout.row()
        row.label(text="Save separate render file:")
        row.prop(props, "separate_render_file")
        if props.separate_render_file :
            row = layout.row()
            row.label(text="Splitted render file:")
            row.prop(props, "render_file")
        row = layout.row()
        row.label(text="Save separate assembly file:")
        row.prop(props, "separate_assembly_file")
        if props.separate_assembly_file :
            row = layout.row()
            row.label(text="Assembly file:")
            row.prop(props, "assembler_file")
        row = layout.row()
        row.label(text="Use relative filepaths:")
        row.prop(props, "use_relative")
