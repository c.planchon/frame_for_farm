import bpy
from bpy import context
from pathlib import Path
import json
import math
import sys
import subprocess
from . scenes_handler import ( sh , keep_params, looped )

def save_props():
    pass

class FFF_OT_prepare(bpy.types.Operator):
    
    bl_idname = "fff.prepare"
    bl_label = "Prepare"
    bl_description = "Prepare Scene Tiles for Rendering by creating a linked scene per tile and appending it to a sequence"
    
    @classmethod
    def poll(cls, context):
        return (context.scene.name not in ['Final','montage'])
     
    def execute(self, context):
        scenizer = sh(context)
        
        if not context.window.scene.fffprops.separate_render_file:
            scenizer.internal_still(context)
        else:
            scenizer.external_still(context)
        if context.scene.fffprops.use_relative:
            bpy.ops.file.make_paths_relative()     
        return {'FINISHED'}

class FFF_OT_assemble(bpy.types.Operator):
    
    bl_idname = "fff.assemble"
    bl_label = "Assemble"
    bl_description = "Cleanup & Merge the previously rendered tiles in one single image"
    
    @classmethod
    def poll(cls, context):
        return (context.window.scene.name != 'Final')
     
    def execute(self, context):
        scenizer = sh(context)
    
        if not context.window.scene.fffprops.separate_assembly_file:
            scenizer.assemble_internal(context)
        else:
            scenizer.assemble_external(context)
        if context.window.scene.fffprops.use_relative:
            bpy.ops.file.make_paths_relative() 
        return {'FINISHED'}
   