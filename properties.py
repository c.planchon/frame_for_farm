import bpy 
from pathlib import Path
from bpy.props import ( StringProperty, EnumProperty, BoolProperty, IntProperty )

from tempfile import TemporaryFile as tmp

class FFFprops(bpy.types.PropertyGroup):

    splits_dir: StringProperty(
        name="",
        description="Folder containing the Textures Images to be imported",
        subtype='DIR_PATH',
        default=str(Path().home()),
    )
    render_file: StringProperty(
        name="",
        description="Filepath of the external render blend",
        subtype='FILE_PATH',
        default=str(Path().home().joinpath("split_render.blend"))
    )
    assembler_file: StringProperty(
        name="",
        description="Filepath of the external assembler blend",
        subtype='FILE_PATH',
        default=str(Path().home().joinpath("assemble_splits.blend"))
    )
    tmp_scenes: StringProperty(
        name="",
        description="list of scenes created temporarily",
        default=""
    )
    fff_all: StringProperty(
        name="",
        description="Addon settings serialized",
        default=""
    )
    file_name: StringProperty(
        name="",
        description="name of the file used to setup the render scene",
        default=""
    )
    frame_current: IntProperty(
        name="",
        description="Include scene animation in splitted render settings",
        default=0
    )
    frame_start: IntProperty(
        name="",
        description="start frame of the original scene to render",
        default=0
    )
    frame_end: IntProperty(
        name="",
        description="end frame of the original scene to render",
        default=0,
    )
    animated: BoolProperty(
        name="",
        description="Include scene animation in splitted render settings",
        default=False
    )
    separate_render_file: BoolProperty(
        name="",
        description="Save split render animation scene in a new file",
        default=False
    )
    separate_assembly_file: BoolProperty(
        name="",
        description="Save assembly scene in a new file",
        default=False
    )
    use_relative: BoolProperty(
        name="",
        description="Use relative paths",
        default=True
    )
    parts: EnumProperty(
        name = "Parts",
        description = "Number of parts",
        items = [
            ("4" , "4" , "Splits the image in 4 parts"),
            ("9" , "9" , "Splits the image in 9 parts"),
            ("16", "16", "Splits the image in 16 parts"),
            ("25", "25", "Splits the image in 25 parts"),
            ("36", "36", "Splits the image in 36 parts"),
            ("49", "49", "Splits the image in 49 parts"),
            ("64", "64", "Splits the image in 64 parts")
        ],
        default = '9'
    )
