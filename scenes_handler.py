import bpy
from bpy import context
from pathlib import Path
import json
import math
import sys
import subprocess

def looped(g):
    def all_parts(*args,**kwargs):
        for i in range(int(kwargs['parts'])):
            kwargs['tile_index'] = i
            g(*args,**kwargs)
    return all_parts 

def animated(g):
    def all_frames(*args,**kwargs):
        kwargs['seq_frame']=0
        if kwargs['animated']:
            for i in range(int(kwargs['frame_start']),int(kwargs['frame_end'])+1):
                kwargs['frame'] = i
                g(*args,**kwargs)
                kwargs['seq_frame']+=1
        else:
            kwargs['frame'] = kwargs['still_frame']
            g(*args,**kwargs)
    return all_frames 

def keep_params(g):
    def all_params(*args,**kwargs):
        
        if kwargs['context'] is None:
            return None
        kwargs['window'] = bpy.data.window_managers[-1].windows[-1]
        all = sh.save_all(**kwargs)
        if all is None:
            return None
        g(*args,**kwargs)
        #kwargs['context'].window.scene.fffprops.fff_all = all
        kwargs['window'].scene.fffprops.fff_all = all
        #sh.load_all(**kwargs)
    return all_params 

def for_each_frame(g):
    def all_frames(*args,**kwargs):
        for frame in range(kwargs['frame']+1,kwargs['frame_end'] - kwargs['frame_start'] + 2):
            frame += kwargs['frame_start']
            kwargs['frame'] = frame
            g(*args,**kwargs)
    return all_frames    

def save_props():
    pass

class sh():
    def __init__(self,context=None):
        self.context = bpy.context if context is None else context
        self.window = bpy.data.window_managers[-1].windows[-1]

    def set_self_vars(self,context=None):
        if context is None:
            context = self.context
        self.frame_current = context.window.scene.fffprops.frame_current    
        if not context.window.scene.fffprops.animated:
            self.frame_current = context.window.scene.fffprops.frame_current = context.window.scene.frame_current
        self.frame_end = context.window.scene.fffprops.frame_end = context.window.scene.frame_end
        self.frame_start = context.window.scene.fffprops.frame_start = context.window.scene.frame_start    
        context.window.scene.render.use_border = True
        self.props = context.window.scene.fffprops
        self.og_name = context.window.scene.name
        self.splits_dir = self.props.splits_dir
        context.view_layer.update()
        self.scene_name = context.window.scene.name
        self.frame_width = bpy.data.scenes[self.scene_name].render.resolution_x
        self.frame_height = bpy.data.scenes[self.scene_name].render.resolution_y
        self.ratio = context.window.scene.render.resolution_percentage
        self.scenes = []
        self.parts = self.props.parts
        splits = int(math.sqrt(int(self.parts)))
        self.y_list = [(((1/splits)*(int(i/splits)),(int(i/splits))*(1/splits)+(1/splits))) for i in range(int(self.parts))]
        self.x_list = [(((1/splits)*(i%splits),(i%splits)*(1/splits)+(1/splits))) for i in range(int(self.parts))]
        self.seq_cursor = 0
        self.still_frame = context.scene.frame_current
            
    def internal_still(self,context=None):
        if context is None:
            context = self.context
        context.window.scene.fffprops.file_name = str(Path(bpy.data.filepath).stem)
        if context.window.scene.fffprops.file_name == "":
            print("Please save your file first")
            return {'CANCELLED'}
        self.set_self_vars(context)
        kwargs = {'i':0,'animated':self.props.animated,'seq_cursor':self.seq_cursor,
                    'still_frame':self.still_frame,'current_scene':context.scene.name,
                    'parts':self.parts,'context':context,'frame':self.frame_current,
                    'frame_start':self.frame_start,'frame_end':self.frame_end,
                    'window': bpy.data.window_managers[-1].windows[-1]
                    }
        kwargs['fff_all'] = context.window.scene.fffprops.fff_all = self.save_all(**kwargs)

        self.bmethod(**kwargs)
        self.make_new_workspace(context)
        self.add_montage_scene(**kwargs)
        self.make_assembly_scene(**kwargs)
        bpy.data.scenes['montage'].fffprops.tmp_scenes = bpy.data.scenes[self.og_name].fffprops.tmp_scenes
        kwargs.update({'i':0,'context':context})
        if kwargs['window'].scene.fffprops.animated:
            #self.add_animated_strips(**kwargs)
            self.set_render_params(**kwargs)
        else:
            self.set_render_params(**kwargs)
        
    def set_scenes_params(self):
        if bpy.data.scenes[self.og_name].fffprops.animated:
            bpy.data.scenes["Final"].frame_end = bpy.data.scenes[self.og_name].frame_end
            bpy.data.scenes["Final"].frame_start = bpy.data.scenes[self.og_name].frame_start
        else:
            bpy.data.scenes["Final"].frame_end = 0
            bpy.data.scenes["Final"].frame_start = 0
        bpy.data.scenes["Final"].fffprops.tmp_scenes = bpy.data.scenes['montage'].fffprops.tmp_scenes 

    #@for_each_frame
    def add_animated_strips(self,**kwargs):
        for frame in range(kwargs['frame']+1,kwargs['frame_end'] - kwargs['frame_start'] + 2):
            frame += kwargs['frame_start']
            kwargs['frame'] = frame
            kwargs['i'] = 0
            self.set_render_params(**kwargs)

    @keep_params
    def add_montage_scene(self,**kwargs):
        kwargs['window'].scene = bpy.data.scenes.new(name='montage')
        kwargs['current_scene'] = kwargs['window'].scene.name
        self.scenes.append(kwargs['current_scene'])
        self.load_all(**kwargs)    
        self.window.scene.render.resolution_percentage = self.ratio
        self.window.scene.render.resolution_x = self.frame_width
        self.window.scene.render.resolution_y = self.frame_height
        self.window.scene.frame_start = 0
        self.window.scene.frame_end = int(self.props.parts) * (1+self.props.animated * (self.frame_end +1- self.frame_start )) - self.props.animated*int(self.props.parts) - 1
        bpy.context.view_layer.update()
        self.window.scene.render.use_border = False
        self.window.scene.render.use_sequencer = True
        bpy.context.view_layer.update()
        self.window.scene.fffprops.fff_all = kwargs['fff_all']
        self.scenes.append(bpy.data.scenes['montage'].name)
        bpy.data.scenes['montage'].fffprops.tmp_scenes = bpy.data.scenes[self.og_name].fffprops.tmp_scenes = self.window.scene.fffprops.tmp_scenes = json.dumps((dict(zip(range(len(self.scenes)), self.scenes))))
        self.window.scene.fffprops.fff_all = self.save_all(**kwargs)

    def assemble_internal(self,context=None):
        self.window.scene = bpy.data.scenes['Final']
        if context is None:
            context = self.context
        if self.window.scene.name != "montage":
            self.window.scene.fffprops.frame_start = self.window.scene.frame_start
            self.window.scene.fffprops.frame_end = self.window.scene.frame_end
            self.window.scene.fffprops.frame_current = self.window.scene.frame_start
        self.frame_start = self.window.scene.fffprops.frame_start 
        self.frame_end = self.window.scene.fffprops.frame_end 
        self.frame_current = self.window.scene.fffprops.frame_start 
        self.props = self.window.scene.fffprops
        self.parts = self.props.parts
        kwargs = {'context':context,'window':bpy.data.window_managers[-1].windows[-1]}    
        props = self.save_all(**kwargs)
        params = json.loads(props)
        scene_name = self.window.scene.name
        renderset = bpy.data.scenes[scene_name].render
        self.frame_width = renderset.resolution_x
        self.frame_height = renderset.resolution_y
        self.ratio = self.window.scene.render.resolution_percentage
        self.splits_dir = self.props.splits_dir
        self.window.scene.fffprops.fff_all = json.dumps(params)
        context.view_layer.update()
        kwargs = {'i':0,'parts':bpy.data.scenes['Final'].fffprops.parts,'animated':self.props.animated,'context':context,'still_frame': 0,'frame':self.frame_start,'frame_start':self.frame_start,'frame_end':self.frame_end}
        #self.load_all(**kwargs)
        
        self.strip_stacker(**kwargs)
        context.view_layer.update()
        self.cleanup_scenes(context)
        context.view_layer.update()
        self.window.scene.render.resolution_percentage = self.ratio
        context.view_layer.update()
        bpy.data.scenes['Final'].frame_set(0)
        return {'FINISHED'}

    def cleanup_scenes(self,context=None):
        #print(bpy.data.scenes['montage'].fffprops.tmp_scenes)    
        if context is None:
            context = self.context
        self.scenes = []
        if not bpy.context.scene.fffprops.separate_assembly_file:
            if bpy.data.scenes['montage'].fffprops.tmp_scenes != "":
                tmp_scenes = json.loads(bpy.data.scenes['montage'].fffprops.tmp_scenes)
                self.scenes = [v for (k,v) in tmp_scenes.items()]
            #print(self.scenes)    
            for scenestodelete in self.scenes :
                if scenestodelete in bpy.data.scenes :
                    bpy.data.scenes.remove(bpy.data.scenes[scenestodelete], do_unlink=True)
        context.view_layer.update()        
        self.window.scene.fffprops.tmp_scenes = ""        
    
    @animated
    @looped
    def strip_stacker(self,**kwargs):
        frame = kwargs['frame']
        tile = f"{self.window.scene.fffprops.file_name}.tile{int(kwargs['tile_index'])+int(kwargs['seq_frame'])*int(kwargs['parts'])} "
        dir_ = Path(self.window.scene.fffprops.splits_dir)
        dir_content = dir_.glob('*.*')
        list_copy = list([x for x in dir_content])
        #print(tile)
        #print([x.stem for x in list_copy])
        # need to test with ==  as it doesn't work with 'is' or 'in'
        v = next(iter([x for x in list_copy if tile.strip().lower() == x.stem.strip().lower()]))
        if kwargs['context'].window.scene.sequence_editor is None :
            kwargs['context'].window.scene.sequence_editor_create()
        strip = kwargs['context'].window.scene.sequence_editor.sequences.new_image(v.stem, str(Path(self.props.splits_dir).joinpath(v.name)), channel=1, frame_start=frame, fit_method='ORIGINAL')
        kwargs['context'].view_layer.update() 
        strip.frame_final_duration = 1
        strip.blend_type = 'ALPHA_OVER'

    @keep_params
    def make_assembly_scene(self,**kwargs):
        context = kwargs['context']
        bpy.data.scenes.new(name='Final')
        kwargs['current_scene'] = 'Final'
        self.load_all(**kwargs)
        kwargs['current_scene'] = context.scene.name 
        #load_all(bpy.context)
        self.window.scene.fffprops.splits_dir = self.splits_dir
        bpy.data.scenes['Final'].frame_start = self.frame_start
        bpy.data.scenes['Final'].frame_end = self.frame_end -1
        bpy.data.scenes['Final'].render.resolution_percentage = self.ratio
        bpy.data.scenes['Final'].render.resolution_x = self.frame_width
        bpy.data.scenes['Final'].render.resolution_y = self.frame_height
        bpy.data.scenes['Final'].render.use_border = False
        bpy.data.scenes['Final'].render.use_sequencer = True
        context.view_layer.update()

        window = self.window
        screen = window.screen
        area = screen.areas[0]
        with bpy.context.temp_override(area=area,screen=screen,window=window):
            context.space_data.view_type = 'SEQUENCER_PREVIEW'
        self.set_scenes_params()

    def external_still(self,context=None):
        if context is None:
            context = self.context
      
        self.window.scene.fffprops.file_name = str(Path(bpy.data.filepath).stem)
        if self.window.scene.fffprops.file_name == "":
            print("Please save your file first")
            return {'CANCELLED'}
        
        self.prepare_render_blend(context)
        subprocess.call([bpy.app.binary_path,'-P',f"{Path(__file__).parent.joinpath('save_render_blend.py')}"])
        Path(__file__).parent.joinpath('save_render_blend.py').unlink()
    
    def assemble_external(self,context=None):
        if context is None:
            context = self.context
        self.window.scene.fffprops.file_name = str(Path(bpy.data.filepath).stem)
        if self.window.scene.fffprops.file_name == "":
            print("Please save your file first")
            return {'CANCELLED'}
        if self.window.scene.name != "montage":
            self.window.scene.fffprops.frame_start = self.window.scene.frame_start
            self.window.scene.fffprops.frame_end = self.window.scene.frame_end
            self.window.scene.fffprops.frame_current = self.window.scene.frame_start
        self.frame_start = self.window.scene.fffprops.frame_start 
        self.frame_end = self.window.scene.fffprops.frame_end
        self.frame_current = self.window.scene.fffprops.frame_start 
        self.props = self.window.scene.fffprops
        self.prepare_assembler_blend(context)
        subprocess.call([bpy.app.binary_path,'-P',f"{Path(__file__).parent.joinpath('save_assembler_blend.py')}"])
        Path(__file__).parent.joinpath('save_assembler_blend.py').unlink()
    
    def prepare_render_blend(self,context=None):
        if context is None:
            context = self.context
        #kwargs = {'context':context,'window':bpy.data.window_managers[-1].windows[-1]}
        self.set_self_vars(context)
        kwargs = {'i':0,'animated':self.props.animated,'seq_cursor':self.seq_cursor,
                    'still_frame':self.still_frame,'current_scene':f"{context.scene.name}",
                    'parts':self.parts,'context':'bpy.context','frame':self.frame_current,
                    'frame_start':self.frame_start,'frame_end':self.frame_end,
                    'window': bpy.data.window_managers[-1].windows[-1],
                    'fff_all': f'{json.loads(self.window.scene.fffprops.fff_all)}',
                    }    
        props = self.save_all(**kwargs)
        data = (f'import bpy \n',f'import sys \n',f'from pathlib import Path', f'import json',
            f'bpy.data.window_managers[-1].windows[-1].scene.name = "delete_me"',
            f'sys.path.append(str(Path("{__file__}").parent))',
            f'from scenes_handler import ( sh , keep_params, looped, animated )',
            
            f'bpy.ops.wm.link(filename="{Path(bpy.data.filepath).name}/Scene/{self.window.scene.name}",directory="{Path(bpy.data.filepath).parent}")',
            f'bpy.data.scenes["{self.window.scene.name}"].make_local()',
            f'bpy.data.scenes.remove(bpy.data.scenes["delete_me"])',
            f"serialized_kwargs='{props}'",
            f'bpy.ops.wm.save_as_mainfile(filepath="{self.window.scene.fffprops.render_file}")',
            
            f'kwargs = json.loads(serialized_kwargs)',
            f'scenizer = sh(bpy.context)',
            f'props = {json.loads(self.window.scene.fffprops.fff_all)}',
            f'bpy.context.window.scene.fffprops.fff_all = json.dumps(props)',
            f'kwargs["window"] = bpy.data.window_managers[-1].windows[-1]',
            f'kwargs["current_scene"] = kwargs["window"].scene.name',
            f'kwargs["fff_all"] = bpy.context.window.scene.fffprops.fff_all' ,
            f'sh.load_all(**kwargs)',
            f'bpy.data.scenes["{self.window.scene.name}"].fffprops.parts = str({self.window.scene.fffprops.parts})',   
            f'scenizer.internal_still(bpy.context)',
            f'bpy.ops.wm.save_as_mainfile(filepath="{self.window.scene.fffprops.render_file}")',
            f'bpy.ops.wm.quit_blender()') 
        with open(f"{Path(__file__).parent.joinpath('save_render_blend.py')}",'w') as py:
            py.write("\n".join(data))

    def prepare_assembler_blend(self,context=None):
        if context is None:
            context = self.context
        #kwargs = {'context':context,'window':bpy.data.window_managers[-1].windows[-1]}
        self.set_self_vars(context)
        kwargs = {'i':0,'animated':self.props.animated,'seq_cursor':self.seq_cursor,
                    'still_frame':self.still_frame,'current_scene':"Final",
                    'parts':self.parts,'context':'bpy.context','frame':self.frame_current,
                    'frame_start':self.frame_start,'frame_end':self.frame_end,
                    'window': bpy.data.window_managers[-1].windows[-1],
                    'fff_all': f'{json.loads(self.window.scene.fffprops.fff_all)}',
                    }    
        props = self.save_all(**kwargs)
        data = (f'import bpy \n',f'import sys \n',f'from pathlib import Path', f'import json',
            f'bpy.data.window_managers[-1].windows[-1].scene.name = "delete_me"',
            f'sys.path.append(str(Path("{__file__}").parent))',
            f'from scenes_handler import ( sh , keep_params, looped, animated )',
            f'bpy.ops.wm.link(filename="{Path(bpy.data.filepath).name}/Scene/Final",directory="{Path(bpy.data.filepath).parent}")',
            f"bpy.data.scenes['Final'].make_local()",
            f'bpy.data.scenes.remove(bpy.data.scenes["delete_me"])',
            #f"serialized_kwargs='{props}'",
            f'bpy.ops.wm.save_as_mainfile(filepath="{self.window.scene.fffprops.assembler_file}")',
            f'kwargs = {{}}',
            f'scenizer = sh(bpy.context)',
            f'props = {json.loads(self.window.scene.fffprops.fff_all)}',
            f'bpy.context.window.scene.fffprops.fff_all = json.dumps(props)',
            f'kwargs["window"] = bpy.data.window_managers[-1].windows[-1]',
            f'kwargs["current_scene"] = kwargs["window"].scene.name',
            f'kwargs["fff_all"] = bpy.context.window.scene.fffprops.fff_all' ,
            f'sh.load_all(**kwargs)',
            f'bpy.data.scenes["Final"].fffprops.parts = str({self.window.scene.fffprops.parts})',   
            f'scenizer.assemble_internal(bpy.context)',
            f'bpy.ops.wm.save_as_mainfile(filepath="{self.window.scene.fffprops.assembler_file}")',
            f'bpy.ops.wm.quit_blender()') 
        with open(f"{Path(__file__).parent.joinpath('save_assembler_blend.py')}",'w') as py:
            py.write("\n".join(data))

    def set_render_params(self,**kwargs):
        kwargs['tile_index'] = 0
        self.add_strips(**kwargs)
        bpy.data.scenes['montage'].render.image_settings.color_mode ='RGBA'
        bpy.data.scenes['montage'].render.image_settings.file_format = 'OPEN_EXR'
        Exportdir = self.props.splits_dir
        Exportfile = f"{kwargs['window'].scene.fffprops.file_name}.tile#.exr"
        Exportstr = str(Path(Exportdir).joinpath(Exportfile))
        bpy.data.scenes['montage'].render.filepath = Exportstr

    
    @animated
    @looped
    def bmethod(self,**kwargs):
        tile_index = kwargs['tile_index']
        frame = kwargs['frame']
        bpy.ops.scene.new(type='LINK_COPY')
        bpy.context.window.scene.name = f"{bpy.context.window.scene.fffprops.file_name}.{int(frame):004d}.tile{tile_index}"
        kwargs['current_scene'] = bpy.context.window.scene.name
        self.scenes.append(kwargs['current_scene'])
        self.load_all(**kwargs)
        bpy.context.window.scene.render.resolution_percentage = self.ratio
        bpy.context.window.scene.render.resolution_x = self.frame_width
        bpy.context.window.scene.render.resolution_y = self.frame_height
        bpy.context.window.scene.frame_start = frame
        bpy.context.window.scene.frame_current = frame
        bpy.context.window.scene.frame_end = frame
        bpy.context.window.scene.render.use_border = True
        bpy.context.window.scene.render.border_min_x = self.x_list[tile_index][0]
        bpy.context.window.scene.render.border_max_x = self.x_list[tile_index][1]
        bpy.context.window.scene.render.border_max_y = self.y_list[tile_index][1]
        bpy.context.window.scene.render.border_min_y = self.y_list[tile_index][0]
    
    def find_largest_region(self,**kwargs):
        largest_region = None
        for area in kwargs['window'].screen.areas:
            for region in area.regions:
                if largest_region is None or region.width > largest_region.width :
                    largest_region = region
        return largest_region
    
    @animated
    @looped
    def add_strips(self,**kwargs):
        tile_index = kwargs['tile_index']
        frame = int(kwargs['frame'])
        #print(f"frame is {frame}")
        offset = int(kwargs['window'].scene.fffprops.parts)*(int(kwargs['window'].scene.fffprops.animated)*frame)
        #print(f"offset set to {offset}")
        framer = int(tile_index) if not kwargs['animated'] else int(tile_index)+int(kwargs['seq_frame'])*int(kwargs['parts']) 
        #print(f"framer set to {framer}")
        tmp_scenes = self.scenes
        scene_name = tmp_scenes[int(tile_index)+int(kwargs['seq_frame'])*int(kwargs['parts'])]
        #print(f"looking for {scene_name}")
        if kwargs['window'].scene.sequence_editor is None :
            kwargs['window'].scene.sequence_editor_create()
            region = self.find_largest_region(**kwargs)
        kwargs['window'].scene.sequence_editor.sequences.new_scene(scene_name, scene=bpy.data.scenes[scene_name], channel=1, frame_start=(framer) )

    @staticmethod
    def save_all(**kwargs):
        #no self only kwargs !
        kwargs['window'] = bpy.data.window_managers[-1].windows[-1]
        args = {}
        args['internals'] = ["type","rna_type","content","bl_rna","name","fff_all"]
        args['attributes'] = [attr for attr in dir(kwargs['window'].scene.fffprops) if attr not in args['internals'] and attr[:2] != "__"]
        for attr in args['attributes']:
            args[attr] = eval(f"kwargs['window'].scene.fffprops.{attr}")
        kwargs['window'].scene.fffprops.fff_all = json.dumps(args)
        return kwargs['window'].scene.fffprops.fff_all
    
    @staticmethod
    def load_all(**kwargs):
        #no self only kwargs !
        props = bpy.data.scenes[kwargs['current_scene']].fffprops   
        args = json.loads(kwargs['fff_all'])
        for attr in args['attributes']:
            setattr(bpy.data.scenes[kwargs['current_scene']].fffprops,attr,args[attr])
            #print(f"attribute {attr} is {getattr(bpy.data.scenes[kwargs['current_scene']].fffprops,attr)}")
        bpy.context.view_layer.update()

        #print(f"parts of {kwargs['current_scene']} is {bpy.data.scenes[kwargs['current_scene']].fffprops.parts} and {bpy.data.scenes[kwargs['current_scene']].fffprops['parts']} ")               
        #bpy.context.view_layer.update()
          
        
    def merge_all_areas(self,context):
        screen = context.screen
        window = context.window
        for area in reversed(list(screen.areas)) :
            if area is not None: 
                if len(list(screen.areas)) > 1:
                    with context.temp_override(area=area,screen=screen,window=window):
                        bpy.ops.screen.area_close()                
               
    def copy_current_area(self):
        bpy.ops.workspace.duplicate()
        #print(bpy.data.workspaces[-1].name)
            
    def split_screen(self):
        #TODO should implement some kind of multi-screen support ?
        screen = bpy.data.window_managers[-1].windows[-1].screen
        window = bpy.data.window_managers[-1].windows[-1]
        area = screen.areas[-1]
        screen.areas[-1].type = 'SEQUENCE_EDITOR'
        if area is not None: 
            #print(area.type)
            with bpy.context.temp_override(area=area,screen=screen,window=window):
                bpy.ops.screen.area_split(direction='VERTICAL', factor=0.83)
                screen.areas[-1].type = 'PROPERTIES' 
            with bpy.context.temp_override(area=screen.areas[-1],screen=screen,window=window):    
                bpy.ops.screen.area_split(direction='HORIZONTAL', factor=0.7) 
                screen.areas[-1].type = 'OUTLINER'  

    def make_new_workspace(self,context):
        current_name = context.workspace.name
        tmp_name = "Tile Assembler"
        if not tmp_name in bpy.data.workspaces :
            context.workspace.name = tmp_name
            self.copy_current_area()
            context.window.workspace = bpy.data.workspaces[f"{tmp_name}.001"]  
            bpy.data.workspaces[f"{tmp_name}.001"].name = current_name  
            self.merge_all_areas(context)
            #context invalidated now
            bpy.data.window_managers[-1].windows[-1].workspace = bpy.data.workspaces[tmp_name]
            self.split_screen()
        #window = bpy.data.window_managers[-1].windows[-1]
        else:
            context.window.workspace = bpy.data.workspaces[tmp_name]
            self.merge_all_areas(context)
            self.split_screen()
        #bpy.context.view_layer.update()
