bl_info = {
    "name": "Frame For Farm",
    "author": "Cosmin Planchon",
    "version": (1, 3, 0),
    "blender": (4, 0, 0),
    "location": "Properties > Render",
    "description": "Splits a still render in multiple parts to ba able to render them on a farm in parallel.",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Render"}

import bpy

from bpy.props import ( PointerProperty, )

from . properties import ( FFFprops, )

from . operators import ( FFF_OT_prepare, FFF_OT_assemble, )

from . panel import ( FFF_PT_Panel, )

classes = (
    FFFprops,
    FFF_OT_prepare,
    FFF_OT_assemble,
    FFF_PT_Panel,
    )

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)
    bpy.types.Scene.fffprops = PointerProperty(type=FFFprops)

def unregister():
    from bpy.utils import unregister_class
    for cls in classes:
        unregister_class(cls)
    del bpy.types.Scene.fffprops
   
if __name__ == '__main__':
    register()
